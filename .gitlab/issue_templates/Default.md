# Titre de la tâche/issue

## Objectifs

Courte description des objectifs de la tâche/issue

## Informations

Toutes informations pertinente pour faciliter la réalisation de la tâche/issue

## Description détaillé

Description detaillé, spécifications, ou expression de besoin.
